#!/bin/zsh

MYIP=`ifconfig en0 inet | grep inet | awk '{ print $2}'`;
echo $MYIP;
REVERSEHOSTNAME=`dig -x $MYIP`;
echo $REVERSEHOSTNAME;
